package main

import (
	"fmt"
	"strconv"
	"time"

	"github.com/tadvi/winc"
)

var tab = [9]int{0, 0, 0, 0, 0, 0, 0, 0, 0}

func main() {
	mainWindow := winc.NewForm(nil)
	mainWindow.SetSize(400, 300)
	mainWindow.SetText("Morpion")
	jeton := 1

	btn1 := winc.NewPushButton(mainWindow)
	btn1.SetText(strconv.FormatInt(int64(1), 10))
	btn1.SetPos((100), 40)
	btn1.SetSize(100, 40)

	btn1.OnClick().Bind(func(e *winc.Event) {
		fmt.Println(jeton)
		if tab[0] == 0 {
			if jeton%2 == 0 {
				btn1.SetText("X")
				fmt.Println("le nombre est pair")
				tab[0] = 1
				test()
			} else {
				btn1.SetText("O")
				fmt.Println("le nombre est impair")
				tab[0] = 2
				test()
			}
			jeton++
		} else {
			fmt.Println("Vous ne pouvez pas ")
		}
		fmt.Println(tab)
	})

	btn2 := winc.NewPushButton(mainWindow)
	btn2.SetText(strconv.FormatInt(int64(2), 10))
	btn2.SetPos((200), 40)
	btn2.SetSize(100, 40)

	btn2.OnClick().Bind(func(e *winc.Event) {
		fmt.Println(jeton)
		if tab[1] == 0 {
			if jeton%2 == 0 {
				btn2.SetText("X")
				fmt.Println("le nombre est pair")
				tab[1] = 1
				test()
			} else {
				btn2.SetText("O")
				fmt.Println("le nombre est impair")
				tab[1] = 2
				test()
			}
			jeton++
		} else {
			fmt.Println("Vous ne pouvez pas ")
		}
		fmt.Println(tab)
	})

	btn3 := winc.NewPushButton(mainWindow)
	btn3.SetText(strconv.FormatInt(int64(3), 10))
	btn3.SetPos((300), 40)
	btn3.SetSize(100, 40)

	btn3.OnClick().Bind(func(e *winc.Event) {
		fmt.Println(jeton)
		if tab[2] == 0 {
			if jeton%2 == 0 {
				btn3.SetText("X")
				fmt.Println("le nombre est pair")
				tab[2] = 1
				test()
			} else {
				btn3.SetText("O")
				fmt.Println("le nombre est impair")
				tab[2] = 2
				test()
			}
			jeton++
		} else {
			fmt.Println("Vous ne pouvez pas ")
		}
		fmt.Println(tab)
	})

	btn4 := winc.NewPushButton(mainWindow)
	btn4.SetText(strconv.FormatInt(int64(4), 10))
	btn4.SetPos((100), 80)
	btn4.SetSize(100, 40)

	btn4.OnClick().Bind(func(e *winc.Event) {
		fmt.Println(jeton)
		if tab[3] == 0 {
			if jeton%2 == 0 {
				btn4.SetText("X")
				fmt.Println("le nombre est pair")
				tab[3] = 1
				test()
			} else {
				btn4.SetText("O")
				fmt.Println("le nombre est impair")
				tab[3] = 2
				test()

			}
			jeton++
		} else {
			fmt.Println("Vous ne pouvez pas ")
		}
		fmt.Println(tab)
	})

	btn5 := winc.NewPushButton(mainWindow)
	btn5.SetText(strconv.FormatInt(int64(5), 10))
	btn5.SetPos((200), 80)
	btn5.SetSize(100, 40)

	btn5.OnClick().Bind(func(e *winc.Event) {
		fmt.Println(jeton)
		if tab[4] == 0 {
			if jeton%2 == 0 {
				btn5.SetText("X")
				fmt.Println("le nombre est pair")
				tab[4] = 1
				test()

			} else {
				btn5.SetText("O")
				fmt.Println("le nombre est impair")
				tab[4] = 2
				test()

			}
			jeton++
		} else {
			fmt.Println("Vous ne pouvez pas ")
		}
		fmt.Println(tab)
	})

	btn6 := winc.NewPushButton(mainWindow)
	btn6.SetText(strconv.FormatInt(int64(6), 10))
	btn6.SetPos((300), 80)
	btn6.SetSize(100, 40)

	btn6.OnClick().Bind(func(e *winc.Event) {
		fmt.Println(jeton)
		if tab[5] == 0 {
			if jeton%2 == 0 {
				btn6.SetText("X")
				fmt.Println("le nombre est pair")
				tab[5] = 1
				test()

			} else {
				btn6.SetText("O")
				fmt.Println("le nombre est impair")
				tab[5] = 2
				test()

			}
			jeton++
		} else {
			fmt.Println("Vous ne pouvez pas ")
		}
		fmt.Println(tab)
	})

	btn7 := winc.NewPushButton(mainWindow)
	btn7.SetText(strconv.FormatInt(int64(7), 10))
	btn7.SetPos((100), 120)
	btn7.SetSize(100, 40)

	btn7.OnClick().Bind(func(e *winc.Event) {
		fmt.Println(jeton)
		if tab[6] == 0 {
			if jeton%2 == 0 {
				btn7.SetText("X")
				fmt.Println("le nombre est pair")
				tab[6] = 1
				test()

			} else {
				btn7.SetText("O")
				fmt.Println("le nombre est impair")
				tab[6] = 2
				test()

			}
			jeton++
		} else {
			fmt.Println("Vous ne pouvez pas ")
		}
		fmt.Println(tab)
	})

	btn8 := winc.NewPushButton(mainWindow)
	btn8.SetText(strconv.FormatInt(int64(8), 10))
	btn8.SetPos((200), 120)
	btn8.SetSize(100, 40)

	btn8.OnClick().Bind(func(e *winc.Event) {
		fmt.Println(jeton)
		if tab[7] == 0 {
			if jeton%2 == 0 {
				btn8.SetText("X")
				fmt.Println("le nombre est pair")
				tab[7] = 1
				test()

			} else {
				btn8.SetText("O")
				fmt.Println("le nombre est impair")
				tab[7] = 2
				test()

			}
			jeton++
		} else {
			fmt.Println("Vous ne pouvez pas ")
		}
		fmt.Println(tab)
	})

	btn9 := winc.NewPushButton(mainWindow)
	btn9.SetText(strconv.FormatInt(int64(9), 10))
	btn9.SetPos((300), 120)
	btn9.SetSize(100, 40)

	btn9.OnClick().Bind(func(e *winc.Event) {
		fmt.Println(jeton)
		if tab[8] == 0 {
			if jeton%2 == 0 {
				btn9.SetText("X")
				fmt.Println("le nombre est pair")
				tab[8] = 1
				test()

			} else {
				btn9.SetText("O")
				fmt.Println("le nombre est impair")
				tab[8] = 2
				test()

			}
			jeton++
		} else {
			fmt.Println("Vous ne pouvez pas ")
		}
		fmt.Println(tab)

	})
	mainWindow.Show()
	mainWindow.OnClose().Bind(wndOnClose)
	winc.RunMainLoop()

}
func wndOnClose(arg *winc.Event) {
	winc.Exit()
}

func test() {
	/////////////LIGNES////////////
	if tab[0]&tab[1]&tab[2] == 1 {
		fmt.Println("X a gagné")
		time.Sleep(5 * time.Second)
		winc.Exit()
	}
	if tab[0]&tab[1]&tab[2] == 2 {
		fmt.Println("O a gagné")
		time.Sleep(5 * time.Second)
		winc.Exit()
	}
	/////////////////////////
	if tab[3]&tab[4]&tab[5] == 1 {
		fmt.Println("X a gagné")
		time.Sleep(5 * time.Second)
		winc.Exit()
	}
	if tab[3]&tab[4]&tab[5] == 2 {
		fmt.Println("O a gagné")
		time.Sleep(5 * time.Second)
		winc.Exit()
	}
	/////////////////////////
	if tab[6]&tab[7]&tab[8] == 1 {
		fmt.Println("X a gagné")
		time.Sleep(5 * time.Second)
		winc.Exit()
	}
	if tab[6]&tab[7]&tab[8] == 2 {
		fmt.Println("O a gagné")
		time.Sleep(5 * time.Second)
		winc.Exit()
	}
	/////////////COLONNES////////////
	if tab[0]&tab[3]&tab[6] == 1 {
		fmt.Println("X a gagné")
		time.Sleep(5 * time.Second)
		winc.Exit()
	}
	if tab[0]&tab[3]&tab[6] == 2 {
		fmt.Println("O a gagné")
		time.Sleep(5 * time.Second)
		winc.Exit()
	}
	/////////////////////////
	if tab[1]&tab[4]&tab[7] == 1 {
		fmt.Println("X a gagné")
		time.Sleep(5 * time.Second)
		winc.Exit()
	}
	if tab[1]&tab[4]&tab[7] == 2 {
		fmt.Println("O a gagné")
		time.Sleep(5 * time.Second)
		winc.Exit()
	}
	/////////////////////////
	if tab[2]&tab[5]&tab[8] == 1 {
		fmt.Println("X a gagné")
		time.Sleep(5 * time.Second)
		winc.Exit()
	}
	if tab[2]&tab[5]&tab[8] == 2 {
		fmt.Println("O a gagné")
		time.Sleep(5 * time.Second)
		winc.Exit()
	}
	/////////////DIAGONALES////////////
	if tab[0]&tab[4]&tab[8] == 1 {
		fmt.Println("X a gagné")
		time.Sleep(5 * time.Second)
		winc.Exit()
	}
	if tab[0]&tab[4]&tab[8] == 2 {
		fmt.Println("O a gagné")
		time.Sleep(5 * time.Second)
		winc.Exit()
	}
	/////////////////////////
	if tab[2]&tab[4]&tab[6] == 1 {
		fmt.Println("X a gagné")
		time.Sleep(5 * time.Second)
		winc.Exit()
	}
	if tab[2]&tab[4]&tab[6] == 2 {
		fmt.Println("O a gagné")
		time.Sleep(5 * time.Second)
		winc.Exit()
	}
}
